import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AuthGuard } from './auth.guard';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { PostResumeComponent } from './post-resume/post-resume.component';
import { SearchComponent } from './search/search.component';
import { ServicesComponent } from './services/services.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';

const routes: Routes = [{ path: 'home', component: HomeComponent },
{ path: 'postresume', component: PostResumeComponent },
{ path: 'service', component: ServicesComponent },
{ path: 'about', component: AboutComponent },
{ path: 'signin', component: SignInComponent },
{ path: 'signup', component: SignUpComponent },
{ path: 'search', canActivate: [AuthGuard], component: SearchComponent },
{ path: 'contact', component: ContactComponent },
{ path: '', redirectTo: 'home', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
