import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PostResumeComponent } from './post-resume/post-resume.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SearchComponent } from './search/search.component';
import { ServicesComponent } from './services/services.component';
import { AboutComponent } from './about/about.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoaderService } from './loader.service';
import { JoblistService } from './joblist.service';
import { GlobalService } from './global.service';
import { SigninService } from './signin.service';
import { PostresumeService } from './postresume.service';
import { ContactComponent } from './contact/contact.component';
import { HttpClientModule } from '@angular/common/http';
import { SignUpComponent } from './sign-up/sign-up.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PostResumeComponent,
    SignInComponent,
    SearchComponent,
    ServicesComponent,
    AboutComponent,
    HeaderComponent,
    FooterComponent,
    ContactComponent,
    SignUpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [PostresumeService, SigninService, GlobalService, JoblistService, LoaderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
