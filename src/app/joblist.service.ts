import { Injectable } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class JoblistService {
  constructor(private http: HttpClient) { }
  api = 'http://localhost:5000/api/v1/';
  getValues(): Observable<Response> {
    let url = 'getjobsearchdetails';
    return from([]);
  }
 
  getList(): Observable<any> {
    let url = this.api + 'users/getjoblist';
    return this.http.get<any>(url);
  }


  getPageData(): Observable<any> {
    let url = this.api + 'users/getpagedata';
    return this.http.get<any>(url);
  }

  getSearchData(searchdata: any): Observable<Response> {
    let url = 'getJobsearchbuttondetails';
    let header = new Headers({ 'Content-Type': 'application/json' });
    return from([]);
  }
  getSearchDatas(parameters: any): Observable<Response> {
    let url = 'getJobsearchbuttondetails';
    let headers = new Headers();
    let srchParams: URLSearchParams = new URLSearchParams();
    for (var key in parameters) {
      if (parameters.hasOwnProperty(key)) {
        let val = parameters[key];

        if (Array.isArray(val)) {
          for (var innerVal in val) {
            srchParams.append(key, val[innerVal]);
          }
        }
        else {
          srchParams.set(key, val);
        }
      }
    }
    console.log('srchParams', srchParams)
    return from([]);
  }

  handleerror(error: Response) {
    return Observable.throw(error);
  }

}