import { TestBed } from '@angular/core/testing';

import { PostresumeService } from './postresume.service';

describe('PostresumeService', () => {
  let service: PostresumeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PostresumeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
