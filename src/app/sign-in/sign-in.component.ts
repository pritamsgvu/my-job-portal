import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SigninService } from '../signin.service';
import { Router } from '@angular/router';
import { GlobalService } from '../global.service';
import { LoaderService } from '../loader.service';

@Component({
  selector: 'app-signin',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  route: any;
  showLoader: boolean = false;
  name: string = '';
  public SignInForm!: FormGroup;
  constructor(private fb: FormBuilder, public globalSvc: GlobalService, private service: SigninService, private router: Router,
    private loaderService: LoaderService, vcr: ViewContainerRef) {
    this.SignInForm = this.fb.group({
      UserName: ['', [Validators.required, Validators.minLength(3)]],
      Password: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

  ngOnInit() {
    this.loaderService.status.subscribe((val: boolean) => {
      this.showLoader = val;
    });
  }
  signIn(): void {
    this.loaderService.display(true);
    let Signindata = this.SignInForm.value;
    let request = { email: Signindata.UserName, password: Signindata.Password };
    this.name = Signindata.UserName;
    this.service.sign(request)
      .subscribe((resp) => {
        console.log('resp',resp);
        this.SignInForm.reset(),
          this.loaderService.display(false);
        this.router.navigate(['/search']);
        this.globalSvc.setUserName(Signindata.UserName);     
      },
        error => {
          this.loaderService.display(false);
          alert(0)
        },
        () => console.log('Authentication Complete')
      );

  }



}
