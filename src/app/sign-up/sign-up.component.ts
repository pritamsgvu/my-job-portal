import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SigninService } from '../signin.service';
import { Router } from '@angular/router';
import { GlobalService } from '../global.service';
import { LoaderService } from '../loader.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  route: any;
  showLoader: boolean = false;
  email: string = '';
  public signUpForm!: FormGroup;
  constructor(private fb: FormBuilder, private globalSvc: GlobalService, private service: SigninService, private router: Router,
    private loaderService: LoaderService, vcr: ViewContainerRef) {
    this.signUpForm = this.fb.group({
      email: ['', [Validators.required, Validators.minLength(3)]],
      password: ['', [Validators.required, Validators.minLength(3)]],
      c_password: ['', [Validators.required, Validators.minLength(3)]],
    });

  }

  ngOnInit() {
    this.loaderService.status.subscribe((val: boolean) => {
      this.showLoader = val;
    });
  }
  signUp(): void {
    this.loaderService.display(true);
    let signUpdata = this.signUpForm.value;
    let request = { email: signUpdata.email, password: signUpdata.password }
    this.email = signUpdata.email;
    this.service.signup(request)
      .subscribe((resp) => {
        console.log('signup', resp)
        this.signUpForm.reset(),
          this.loaderService.display(false);
        this.router.navigate(['/signin']);
      },
        error => {
          this.loaderService.display(false);
          alert(0)
        },
        () => console.log('User Created')
      );

  }



}
