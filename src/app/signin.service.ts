import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable()
export class SigninService {
  userData: any;
  constructor(public http: HttpClient) { }
  api = 'http://localhost:5000/api/v1/';
  sign(data: any): Observable<any> {
    let url = this.api + 'employees/signin';
    return this.http.post<any>(url, data);
  }

  signup(data: any): Observable<any> {
    let url = this.api + 'employees';
    return this.http.post<any>(url, data);
  }

  handleerror(error: Response) {
    return Observable.throw(error.statusText);
  }

}